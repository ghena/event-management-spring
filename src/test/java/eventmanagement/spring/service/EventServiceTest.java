package eventmanagement.spring.service;

import static org.mockito.Mockito.when;

import eventmanagement.spring.controller.EventDto;
import java.util.List;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class EventServiceTest {

  @InjectMocks
  private EventService eventService;

  @Mock
  private EventRepository eventRepository;

  @Test
  void shouldGetEvents() {
    // Given
    when(eventRepository.getAllEvents()).thenReturn(List.of(new EventDto()));

    // When
    List<EventDto> events = eventService.getAllEvents();

    // Then
    Assertions.assertEquals(1, events.size(), "There should be 1 events");
  }
}
