package eventmanagement.spring.service;

import eventmanagement.spring.controller.EventDto;
import java.util.List;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@AllArgsConstructor
public class EventService {

  private final EventRepository eventRepository;

  public List<EventDto> getAllEvents() {
    return eventRepository.getAllEvents();
  }

}
