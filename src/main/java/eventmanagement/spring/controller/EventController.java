package eventmanagement.spring.controller;

import eventmanagement.spring.service.EventService;
import java.util.List;
import javax.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/v1/events")
@Slf4j
@RequiredArgsConstructor
public class EventController {

  private final EventService eventService;

  @PostMapping(
      consumes = MediaType.APPLICATION_JSON_VALUE,
      produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<?> saveEvent(@RequestBody @Valid EventDto event) {
    // mock
    return ResponseEntity.status(HttpStatus.CREATED).build();
  }

  @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
  public List<EventDto> getAllEvents() {
    log.info("EM: Get events");
    return eventService.getAllEvents();
  }
}
